{ flat, ...}: # these can be used in permanent package sets, or can be accessed ephemerally
#    by doing: `nix develop '.#media.creating'` for example
{
  unfree = s: u: # {
  with s; [
  # steam                     # using nixos module instead
  # lutris
  heroic
  # NOTE: OVE-...
  prismlauncher             # minecraft isn't FOSS
  s.obsidian                # loqseq just doesn't cut it
  ]; #}
  core = s: u: # {
  with u; [
  bitwarden                 # password manager, it's that simple :)
  nitrogen                  # wallpaper manager
  # picom                     # compositor
  xsecurelock               # secure lockscreen
  xss-lock                  # lockscreen as screensaver
  slock                     # locker
  ]; #}
  # NOTE this contains unfree cuda references, and does not protect against
  # accidental installation of proprietary software, use this wisely
  compute = s: u: # {
  with s; [
    u.blender               # latest & greatest 3D modeling
    openai-whisper-cpp      # manage transcriptions
  ]; #}
  etc = s: u: # {
  with u; [
  s.junction                # browser selector
  s.firefox                 # decent backup
  s.librewolf               # primary browser
  s.chromium                # backup browsers
  # browsh                    # TUI browser
  thunderbird               # Mozillas mail client
  # mailspring                # email client
  libreoffice               # office suite
  # godot                     # game engine
  legendary-gl #Bheroic     # "epic" game yoinking
  pidgin                    # chat client
  tut                       # TUI mastodon client
  element-desktop           # matrix client
  cinny                     # matrix client
  emote                     # emoji picker
  ]; #}
  code = s: u: # {
  with s; [
  nim                       # nim compiler
  jdk                       # java runtime
  # python27                  # python interpreter 2.7
  python3                   # python interpreter 3.*
  poetry                    # python package-manager
  yarn                      # JS dep-manager | coc.vim needs it

  cabal2nix                 # haskell packager
  ]; #}
  editors = s: u: # {
  with s; [
  # nvimpager                 # why not?
  # amp kakoune               # similar to vim
  # micro                     # similar to nano
  # notepadqq                 # GUI text editor
  nano gnome.gedit          # basic AF editor
  # languagetool              # proofread tool
  jetbrains.idea-community  # java is cursed, this makes it less so
  ]; #}
  fonts = s: u: # {
  with u; [
  (nerdfonts.override {
    fonts = [
    "Mononoki"
    "Meslo"
    "FiraCode"
    "Terminus"
    ];
  })

  noto-fonts
  noto-fonts-cjk
  noto-fonts-emoji
  liberation_ttf
  #s.mplus-outline-fonts
  dina-font
  (hiPrio proggyfonts)
  uw-ttyp0
  tamzen
  powerline-fonts
  ]; #}
  extras = s: u: # {
  with u; [
  sl                        # to fix ls typos, of course
  # bsod                      # extremely powerful windows emulator
  pipes                     # line-doodles
  breeze-icons              # icon theme
  libsForQt5.qtstyleplugins # let QT themes apply properly
  adwaita-qt                # blend QT into gnome's style
  qt5ct                     # theming for stuff inside xorg
  xsettingsd                # application settings, alternative to gsettingsd
  lxappearance              # theming
  touchegg                  # touch gestures
  ]; #}
  # TODO switch to microvm / declarative virtual machines
  vm = s: u: # {
  with u; [
  # anbox                     # Android APK runner
  # virt-manager              # VM manager
  # virtualbox                # VM host
  qemu-utils                # qemu tools
  (hiPrio qemu)             # VM method
  qemu_kvm                  # VM qumu bridge
  # kvm                       # VM method
  # libguestfs                # for accessing guest fs's
  # FIXME quick-emu stuff from failed packaging attempt
  # IDK, kvm stuff...
  bridge-utils
  # virt-viewer
  dnsmasq
  vde2
  netcat
  edk2
  OVMF
  # OVMFFull
  libvirt
  # virtlyst
  # swtpm
  # swtpm-tpm2
  ]; #}
  # TODO slowly phase out in favor of wl!
  xorg = s: u: # {
  (with u.xorg; [
  xinit                   # to start x
  libXrandr               # res change lib
  xrandr                  # change your resolution
  xprop                   # x properties
  xwininfo                # like xprop
  xev                     # input tester
  ]) ++
  (with u; [
  autorandr                 # display manager
  xdotool                   # virtual keypress util
  wmctrl                    # window manager functions
  # greetd.greetd             # greeter!
  # greetd.tuigreet           # greeter!
  # greetd.dlm                # greeter!
  # greetd.gtkgreet           # greeter!
  ]); #}
  hacking = s: u: # {
  with u; [
# torbrowser                # heck yeah
# cutter                    # cut TCP/IP connections
  wireshark                 # monitor network packages
  ]; #}
  games = s: u: # {
  with u; [
  (hiPrio dolphin-emu-primehack)
  dolphinEmuMaster          # wii/gamecube emulator (near-git version)
  rpcs3                     # ps3 emulator
  vitetris                  # epic terminal-based tetris game
  superTuxKart              # super-tux-kart!
  xonotic-glx               # gud FPS
  # _2048-in-terminal
  # _0verkill
  # cuyo
  ]; #}
  misc = s: u: # {
  with u; [
  bit diff-so-fancy         # git stuff
  # nodejs                    # TODO: integrate w/ neovim
  picom-next
  xorg.xmodmap
  kdeconnect
  libnotify
  alsa-utils
  ]; #}
  experiments = s: u: # {
  with u; [
  soft-serve
  # undistract-me # pacman
  bottles quickemu proton-caller
  prusa-slicer
  snapmaker-luban
  bambu-studio
  appimage-run
  weston                    # basic wayland compositor, for mucking and dev
  jay                       # wayland compositor, like XMonad, but rusty

  distrobox

#  (u.buildFHSUserEnv {name = "pacman";
#  targetPkgs = pkgs: (with pkgs;
#    [ udev
#      pacman
#    ]);
#  multiPkgs = pkgs: (with pkgs;
#    [ udev
#    ]);
#  runScript = "pacman";
#}).env


  # temp etc
  # sqlite-interactive
  mate.engrampa
  #busybox
  zip unzip
  xclip

  # cmake                     # for source builds
  # binutils                  # for source builds
  # gcc                       # compiler

  # goldberg-emu              # separate things from steam :)
  ]; #}
  haskell = s: u: # {
  with u; [(
  # ghcWithHoogle > ghcWithPackages -> haddock
    let f = haskell.lib.appendConfigureFlag;
    in u.haskellPackages.ghcWithHoogle (p: with p; [
  xmonad                       # wm
  xmonad-contrib               # xmonad stuff
  # xmonad-extras                # xmonad-contrib++ # needs transition to flakes
  xmonad-utils                 # xmonad more more stuff stuff
  # DescriptiveKeys              # xmonad stuff more in the more of the stuff
(f xmobar "-f all_extensions") # xmobar addition for xmonad
  # control-bool

  random-fu                    # functions for use in xmonad
  # text                         # useful functions for working w/ json

  # yi-custom                    # haskellEditor
  ])
  )]; #}
  media = {
  managing = s: u: # {
    with u; [
    pavucontrol               # pulseaudio manager
    playerctl                 # manage media easier
  # easyeffects               # apply effects to audio in pw
  ]; #}
  capturing = s: u: # {
    with u; [
    maim                      # screenshot tool
    scrot                     # super simple screenshot utility
    flameshot                 # screenshot utility
    capture                   # A "no BS" CLI simple screen capture utility
    obs-studio                # OBS itself
    # obs-move-transition       # OBS move items upon transition
    # obs-wlrobs                # OBS wayland screen captures
  ]; #}
  creating = s: u: # {
    with u; [
    # blender                   # 3d editor ftw
    cura                      # 3d slicer
    prusa-slicer              # 3d slicer w/ resin support
    avidemux                  # a very basic video editor
#B  olive-editor              # a more complex video editor
    (flat s "org.kde.kdenlive") # for now
    (flat s "org.openshot.OpenShot")    # for now
#B  kdenlive                  # a more complex video editor
#B  mediainfo                 # ^ unlisted dependency
#B  openshot-qt               # a more complex video editor
    ardour                    # music.mp3
    audacity                  # modify.mp3
    gimp                      # modify.img
    krita                     # paint.img
    akira-unstable            # SVGs.img
  ]; #}
  viewing = s: u: # {
    with u; [
    # TODO: LBRY
    celluloid mpv vlc         # multimedia players
    # minitube yt-dlp           # view youtube videos
    gpodder vocal             # podcast clients

  #Bnewsflash                 # RSS reader
  ]; #}
  };
  utils = {
  terminals = s: u: # {
    with u; [
    st                        # my terminals
    # alacritty                 # my terminals
  ];
  #}
  debuggers = s: u: # {
    with u; [
    cgdb gdb pwndbg edb       # general debuggers suite :)
    bench                     # CLI benchmarking tool
  # pydb                      # python debugger
    bashdb                    # bash debugger
    delve                     # golang debugger
  ];
  #}
  security = s: u: # {
    with u; [
    # open VPN solutions
    openvpn gnome.networkmanager-openvpn
    # proton vpn solution
    protonvpn-cli #protonvpn-gui
    # wireguard vpn stuff
    boringtun wg-bond wireguard-tools
    firejail                  # sandbox applications, particularly useful with web browsers
    usbguard                  # anti badusb
    i2p tor torsocks          # online protection
    macchanger                # randomize your mac address
    s.vulnix                  # vuln scanner
    sshguard                  # anti ssh attacks
    ferm                      # complex firewall management
    firehol                   # "a firewall for humans"
    s.opensnitch              # firewall for applications
    opensnitch-ui             # another firewall??
  ];
  #}
  shells = s: u: # {
    with u; [
    any-nix-shell             # connect nix-shell with yours
    # zsh                       # Zsh because it's useful I guess...
    bash bashInteractive      # Bash shell for easier scripting
  ];
  #}
  cli = s: u: # {
    with u; [
    bottom htop               # system monitors
    ftop                      # file-top
    nix-top                   # nix watcher
    nil                       # nice nix-lang extension

    hostname                  # the ability to modify your hostname | un-nix?
    bat eza fd bfs            # better coreutils in rust
    lzip gzip bzip2 xz        # zippers
    curl wget                 # downloaders
    more less moar            # pagers
    git git-annex gitui       # version control + file management solution + UI
    wally-cli qmk             # mechanical keyboard stuff
    # TODO make .desktop wrapper for opening this in file manager
    archivemount              # mount archives (via FUSE)

    xclip                     # clipboard util

    rsync                     # powerful file copy/share
    ffsend                    # file share for use with FF
    fzf                       # fuzzy finder tool
    btrfs-progs               # betterfs tools
    screen                    # multiplex/session manager
    neofetch pfetch           # show off your system
    pandoc                    # conversion tool

    # flootty                   # collab TTY
  ];
  #}
  gui = s: u: # {
    with u; [
    # xdragon                   # drag-n-drop utility
    czkawka                   # file duplicate management tool
    transmission-gtk          # torrent tool! also has cli
    sxiv                      # simple x image viewer
    # TODO integrate these 3 into window manager
    clipit                    # clipboard manager
    xzoom                     # screen magnifier   : These work well
    colorpicker               # hex value of pixel : together.
    brightnessctl             # manage brightness
#   remmina                   # remote connection client
    cinnamon.warpinator       # LAN file sharer
    # TODO troubleshoot gvfs
    xfce.thunar gvfs          # file manager + trash support
    usermount                 # automatically mount drives as they appear
    conky                     # snazzy
    deadd-notification-center # notification system
    gxmessage                 # dialog system
    caffeine-ng               # stop screen-locking
    blueman                   # bluetooth tray
    networkmanagerapplet      # network tray
    stalonetray               # better system tray
#   trayer                    # system tray
    mate.engrampa             # Archive tool GUI
#   ark                       # Archive tool GUI
    qalculate-gtk             # nice calculator
    kmplot                    # plotting software (graphing calc)
    tilem                     # TI# calculator emulator (needs ROM file)
    gnome.dconf-editor        # dconf editor, duh
    # drawing                   # basic drawing tool - like G-drawings
    drawio                    # flowcharts!!!!
];
#}
  aliens = s: u: # {
    with u; [
    nix-alien
    nix-index-update
];
#}
  };
  system = s: u: # {
  with u; [
  # {
  alacritty neovim vim helix# terminal & essential editors
  fish dash bash            # main shell & sh + bash, for compatability
  wget curl git
  # TODO transition to doas exclusively
  sudo doas
  qutebrowser
  # TODO place in wm config
  nitrokey-app              # nitrokey!
  nitrokey-udev-rules       # ^ w/out root
  #}
  # nix stuff {
  nixos-install-tools       # for complete dominion of all the drives!
  nix-bundle                # https://github.com/matthewbauer/nix-bundle
  nix-diff                  # explain why two nix derivations differ
  cachix                    # never build software twice!
  # attic                     # local nixos cache
  #}
  # core system {
  btrfs-progs cryptsetup
  coreutils                 # don't piss GNU+Stallman off!
  libtool                   # more gnu tools
  lsof strace               # identify open files
  usbutils                  # lsusb+
  #}
  # userspace {
  xterm                     # basic terminal, as a backup only
  libsecret                 # secret storage
  gnome.gnome-keyring       # Keyring, manages DBs other than pw-managers
  gnome.libgnome-keyring    # secret storage
  gnome.seahorse            # secret management
  #}
  # resource management {
  auto-cpufreq              # dynamic CPU frequency scaler
  earlyoom                  # oomKiller sucks! But this doesn't.
  freefall                  # anti HDD death
  #}
  ];
  #}
}
