{
  description = "My new NixOS configuration";
  inputs = {
    unstable .url = "github:nixos/nixpkgs/nixos-unstable";
    stable   .url = "github:nixos/nixpkgs/nixos-23.05";
    home-manager={
      url = "github:nix-community/home-manager/release-23.05";
      inputs.nixpkgs.follows = "stable";};
    impermanence.url = "github:nix-community/impermanence";
      # TODO: when 22.05 isn't latest, upgrade
      xmonad={
          url = "github:xmonad/xmonad/efffa8946ac68960af60a1fb22df367023d03fe0";
          inputs.nixpkgs.follows = "stable";
      };
      xmonad-contrib={
          url = "github:xmonad/xmonad-contrib/7062b75ea993fd423a46fbb1976bc4ecabb065aa";
          inputs.xmonad.follows = "xmonad";
          inputs.nixpkgs.follows = "stable";
      };
   # nix-alien.url = "github:thiagokokada/nix-alien";
   # nix-ld.url = "github:Mic92/nix-ld/main";

    #submodules={url = "file:///./submodules";
    #  type="git"; submodules=true;
    #  inputs.nixpkgs.follows = "stable";};
  };
  outputs = inputs: with inputs;
    let # basics {
      libs = { lib = stable.lib; };
      default = "free";
      overlays = {overlays = [
        xmonad-contrib.overlay
      ];};
      primary = import ./primary.nix {}; # username/etc
      utility = import ./utility.nix (inputs // primary // libs // overlays);
    in with utility; with primary; let #}
      # inputs {
      unfree = enslave unstable;
      locked = enslave   stable;
      free   = ingest  unstable;
      frozen = ingest    stable;
      uncuda = crunch  unstable;
      cuda   = crunch    stable;
      #}
      # args = inputs + primary + utility {
      most = inputs // primary // {
        # inputs
        inherit inputs
          free   frozen
          unfree locked
          uncuda cuda
          ;
        # other
        inherit utility;
      };
      bundles = import ./bundles.nix { inherit flat; };
      args = most // { default=most."${default}"; inherit bundles; };
      #}
  main = makeAtHome rec {
   inherit (primary) hostname;
   standalone = true;
   home = {
     inherit args;
     pkgs = stable.legacyPackages."${system}";
     stateVersion = "22.05";

     users = {
       "${user}" = [ ./home.nix ];
     };
   };

   config = {
     "${hostname}" = {
       specialArgs = args // libs;
       modules = [
         # manage volumes setup by format.sh and generated by nix
         (volumes    ./volumes)
         # default population of users first, system may override if need be
         (population home.users [ "lxd" "vboxusers" "vboxsf" "libvirtd" "kvm" ])
         # the rest of the system config
         ./system.nix
         # manage persistent state
         ./persist
         # binary caches
         ./cachix.nix

         # other modules
         # (for nix-alien)

       ] ++
         # 
         xmonad-contrib.nixosModules
         ++ [
         xmonad-contrib.modernise.${primary.system}
         ]
       ;
     };
   };
  # create bundles as devshells (see bundles.nix#L1-L2)
  devShells."${system}" =
  bundle "" frozen free bundles
  # // (bundle "" frozen nix-alien.packages."${system}" { inherit (bundles.utils) aliens; })
  # // { pacman = (args.free.buildFHSUserEnv {name = "pacman";
  # targetPkgs = pkgs: (with pkgs;
  #   [ udev
  #     pacman
  #     alacritty
  #   ]);
  # multiPkgs = pkgs: (with pkgs;
  #   [ udev
  #     pacman
  #   ]);
  # runScript = "bash";
  # }).env; }
  ;

  };
  in main // {
    # nix build -> primary home-manager configuration
    packages."${system}".default =
    if main.standalone then
      main."${user}".activationPackage
    else
      throw ''
      Did you mean: `doas nixos-rebuild --flake .`?
      Not building home-manager module.
      Set (standalone=true) to allow this.
      ''
    ;
    };
}
